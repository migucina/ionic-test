import {Component, OnInit} from '@angular/core';
import {NavController} from 'ionic-angular';
import {Page} from '../page/page';
import {LocalStorageProvider} from '../../providers/local-storage/local-storage';
import * as $ from 'jquery';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  theme: boolean = false;

  constructor(public LS: LocalStorageProvider, public navCtrl: NavController) {
    this.theme = LS.getTheme();
  }

  ngOnInit() {
  }

  open() {
    this.navCtrl.push(Page);
    this.navCtrl.remove(0, 1);
  }
}
