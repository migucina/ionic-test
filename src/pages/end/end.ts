import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {LocalStorageProvider} from "../../providers/local-storage/local-storage";
import {HomePage} from "../home/home";

/**
 * Generated class for the EndPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-end',
  templateUrl: 'end.html',
})
export class EndPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private LS:LocalStorageProvider,private platform: Platform) {
  }

  ionViewDidLoad() {
  }


  closeApp():void{
    console.log("App close");
    this.platform.exitApp();
  }

  toMenu():void{
    this.navCtrl.push(HomePage);
    this.navCtrl.remove(0, 1);
  }
}
