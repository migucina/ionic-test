import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {LocalStorageProvider} from "../../providers/local-storage/local-storage";
import {HomePage} from "../home/home";
import {EndPage} from "../end/end";

import * as $ from 'jquery';

/**
 * Generated class for the Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-page',
  templateUrl: 'page.html',
})
export class Page {
  NumberPage: number = 0;
  MaxNumberPage: number = 0;
  Data: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public LS: LocalStorageProvider) {

  }

  ionViewDidLoad() {
    this.getPage();
    this.MaxNumberPage = this.LS.getMaxIndex();
    console.log(this.MaxNumberPage)
  }

  getPage() {
    this.Data = this.LS.getPgaeByIndex(this.NumberPage);
  }

  runAnimation(type: string) {
    $("#box-animation").attr('class', '');
    $("#box-animation").addClass('animation');
    $("#box-animation").addClass(`out-${type}-box`);
    setTimeout(() => {
      this.getPage();
      $("#box-animation").removeClass(`out-${type}-box`).addClass(`in-${type}-box`);
    }, 250);
  }

  nextPage() {
    if (this.MaxNumberPage > this.NumberPage) {
      this.NumberPage++;
      this.runAnimation('n');
    }
    else{
      this.navCtrl.push(EndPage);
      this.navCtrl.remove(0, 1);
    }
  }

  backPage() {
    if (this.NumberPage > 0) {
      this.NumberPage--;
      this.runAnimation('b');
    }
    else{
      this.navCtrl.push(HomePage);
      this.navCtrl.remove(0, 1);
    }
  }



}
