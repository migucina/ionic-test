import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

/*
  Generated class for the LocalStorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocalStorageProvider {
  THEME: boolean = false;

  DATA: any;


  constructor(private http: HttpClient) {
    this.http.get(`assets/data.json`).subscribe(rep => this.DATA = rep);
  }

  setTheme(val: boolean) {
    this.THEME = val;
  }
  getTheme() {
    return this.THEME;
  }

  getNameTheme() {
    return this.THEME ? 'red' : 'blue';
  }

  getPgaeByIndex(index: number): any {
    return this.DATA[index];
  }

  getMaxIndex(): number {
    return this.DATA.length- 1;
  }
}
