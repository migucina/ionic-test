import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {Page} from '../pages/page/page';
import {EndPage} from "../pages/end/end";
import {LocalStorageProvider} from '../providers/local-storage/local-storage';
import {ScreenOrientation} from '@ionic-native/screen-orientation';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Page,
    EndPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    EndPage,
    Page
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LocalStorageProvider,
    ScreenOrientation
  ]
})
export class AppModule {
}
